const mongoose = require('mongoose')
const { Schema } = mongoose

const notesSchema = new Schema({
  userId: {
    type: String,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: [true, 'Set text of the note'],
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
})

const Note = mongoose.model('notes', notesSchema)

module.exports = { Note }
