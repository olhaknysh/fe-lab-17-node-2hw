const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const fs = require('fs')
const path = require('path')
const morgan = require('morgan')

const authRouter = require('./routes/api/auth')
const usersRouter = require('./routes/api/users')
const notesRouter = require('./routes/api/notes')
const { errorHandler } = require('./helpers/apiHelpers.js')
const { notFoundHandler } = require('./helpers/apiHelpers.js')

const app = express()
const accessLogStream = fs.createWriteStream(
  path.join(__dirname, 'access.log'),
  {
    flags: 'a',
  }
)

app.use(morgan('combined', { stream: accessLogStream }))
app.use(cors())
app.use(express.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.use('/api/auth', authRouter)
app.use('/api/users', usersRouter)
app.use('/api/notes', notesRouter)

app.use(notFoundHandler)
app.use(errorHandler)

module.exports = app
