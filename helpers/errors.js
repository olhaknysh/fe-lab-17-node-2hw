class CustomError extends Error {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class userWithSuchUsername extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class notAuthorized extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class noCredentials extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class PasswordError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class DeleteError extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

class NotFound extends CustomError {
  constructor(message) {
    super(message)
    this.status = 400
  }
}

module.exports = {
  CustomError,
  notAuthorized,
  userWithSuchUsername,
  noCredentials,
  PasswordError,
  DeleteError,
  NotFound,
}
