const express = require('express')
const router = express.Router()

const {
  getUserController,
  deleteUserController,
  changeUserController,
} = require('../../controllers/auth')

const { asyncWrapper } = require('../../helpers/apiHelpers')
const { authMiddleware } = require('../../middlewares/authMiddleware')

router.get('/me', authMiddleware, asyncWrapper(getUserController))
router.delete('/me', authMiddleware, asyncWrapper(deleteUserController))
router.patch('/me', authMiddleware, asyncWrapper(changeUserController))

module.exports = router
