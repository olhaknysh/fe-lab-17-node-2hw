const express = require('express')
const router = express.Router()

const {
  getNotesController,
  addNoteController,
  getNoteByIdController,
  updateNoteByIdController,
  updateCheckFieldByIdController,
  deleteNoteByIdController,
} = require('../../controllers/notes')

const { asyncWrapper } = require('../../helpers/apiHelpers')
const { authMiddleware } = require('../../middlewares/authMiddleware')

router.get('/', authMiddleware, asyncWrapper(getNotesController))
router.post('/', authMiddleware, asyncWrapper(addNoteController))
router.get('/:id', authMiddleware, asyncWrapper(getNoteByIdController))
router.put('/:id', authMiddleware, asyncWrapper(updateNoteByIdController))
router.patch(
  '/:id',
  authMiddleware,
  asyncWrapper(updateCheckFieldByIdController)
)
router.delete('/:id', authMiddleware, asyncWrapper(deleteNoteByIdController))

module.exports = router
