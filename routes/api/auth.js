const express = require('express')
const router = express.Router()

const {
  registerUserController,
  loginUserController,
} = require('../../controllers/auth')

const { asyncWrapper } = require('../../helpers/apiHelpers')

router.post('/register', asyncWrapper(registerUserController))
router.post('/login', asyncWrapper(loginUserController))

module.exports = router
