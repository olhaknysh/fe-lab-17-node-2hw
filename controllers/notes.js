const {
  getNotes,
  addNote,
  getNoteById,
  updateNote,
  updateCheckField,
  deleteNote,
} = require('../services/notesService')
const { noCredentials} = require('../helpers/errors')

const getNotesController = async (req, res) => {
  const { _id } = req.user
  const { offset, limit } = req.query

  const { count, notes } = await getNotes(_id, offset, limit)

  res.status(200).json({
    offset: offset ? Number(offset) : 0,
    limit: limit ? Number(limit) : 0,
    count,
    notes,
  })
}

const addNoteController = async (req, res) => {
  const { _id } = req.user
  const { text } = req.body
  if (!text) {
    throw new noCredentials('Please enter text of the note')
  }

  await addNote(_id, text)

  res.status(200).json({ message: 'Success' })
}

const getNoteByIdController = async (req, res) => {
  const { id } = req.params
  const note = await getNoteById(id)
  res.status(200).json({ note: note[0] })
}

const updateNoteByIdController = async (req, res) => {
  const { id } = req.params
  const { text } = req.body

  if (!text) {
    throw new noCredentials('Please enter text of the note')
  }

  await updateNote(id, text)

  res.status(200).json({ message: 'Success' })
}

const updateCheckFieldByIdController = async (req, res) => {
  const { id } = req.params

  await updateCheckField(id)

  res.status(200).json({ message: 'Success' })
}

const deleteNoteByIdController = async (req, res) => {
  const { id } = req.params

  await deleteNote(id)

  res.status(200).json({ message: 'Success' })
}

module.exports = {
  getNotesController,
  addNoteController,
  getNoteByIdController,
  updateNoteByIdController,
  updateCheckFieldByIdController,
  deleteNoteByIdController,
}
