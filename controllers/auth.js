const {
  registerUser,
  loginUser,
  updateUser,
  removeUser,
} = require('../services/userService')
const { noCredentials, PasswordError } = require('../helpers/errors')

const registerUserController = async (req, res) => {
  const { username, password } = req.body
  if (!username || !password) {
    throw new noCredentials('Please provide username and password fields')
  }
  await registerUser(username, password)

  res.status(200).json({ message: 'Success' })
}

const loginUserController = async (req, res) => {
  const { username, password } = req.body
  if (!username || !password) {
    throw new noCredentials('Please provide username and password fields')
  }
  const jwt_token = await loginUser(username, password)

  res.status(200).json({ message: 'Success', jwt_token })
}

const getUserController = async (req, res) => {
  const { username, _id, createdDate } = req.user

  const user = {
    _id: _id.toString(),
    username,
    createdDate,
  }
  res.status(200).json({ user })
}

const changeUserController = async (req, res) => {
  const { _id } = req.user
  const { oldPassword, newPassword } = req.body

  if (oldPassword === newPassword) {
    throw new PasswordError('New password is the same as the old one')
  }

  if (!oldPassword || !newPassword) {
    throw new noCredentials(
      'Please provide old password and new password fields'
    )
  }

  await updateUser(_id, oldPassword, newPassword)

  res.status(200).json({ message: 'Success' })
}

const deleteUserController = async (req, res) => {
  const { _id } = req.user

  await removeUser(_id)

  res.status(200).json({ message: 'Success' })
}

module.exports = {
  registerUserController,
  loginUserController,
  getUserController,
  deleteUserController,
  changeUserController,
}
