# Notes API

**_Write down your notes_**

## Overview

This API will help you with:

- getting list of notes
- getting certain note by Id
- adding notes
- changing notes
- deleting notes
- ckeck/uncheck status of note

## Getting started

Please use `http://yourhostname/api/`

## Commom routes

`post 'auth/register'`

Register the user

`post 'auth/login'`

Login the user

## Routes that need authorization

`get 'users/me`

Get info about current user

`patch 'users/me'`

Change password

`delete 'users/me'`

Delete current user from the database

### Notes

`get 'notes'`

Get list of all notes

`post 'notes'`

Add new note

`get 'notes/:id'`

Get specific note by id

`put 'notes/:id'`

Change text of the note

`patch 'notes/:id'`

Change field completed to the opposite

`delete 'notes/:id'`

Delete note
