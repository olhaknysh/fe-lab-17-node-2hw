const ObjectId = require('mongodb').ObjectId
const { Note } = require('../model/notesModal')
const { NotFound } = require('../helpers/errors')

const getNotes = async (userId, offset, limit) => {
  const allNotes = await Note.find({ userId })
  const notes = await Note.find({ userId })
    .skip(Number(offset))
    .limit(Number(limit))

  return { count: allNotes.length, notes }
}

const addNote = async (userId, text) => {
  const note = new Note({
    userId,
    text,
  })
  await note.save()
}

const getNoteById = async (id) => {
  try {
    const note = await Note.find({ _id: new ObjectId(id) })
    if (note.length === 0) {
      throw new Error()
    }
    return note
  } catch (err) {
    throw new NotFound(`Note with id ${id} is not found`)
  }
}

const updateNote = async (id, text) => {
  try {
    await Note.findOneAndUpdate({ _id: new ObjectId(id) }, { text })
  } catch (err) {
    throw new NotFound(`Note with id ${id} is not found`)
  }
}

const updateCheckField = async (id) => {
  try {
    await Note.updateOne({ _id: new ObjectId(id) }, [
      { $set: { completed: { $eq: [false, '$completed'] } } },
    ])
  } catch (err) {
    throw new NotFound(`Note with id ${id} is not found`)
  }
}

const deleteNote = async (id) => {
  await Note.findByIdAndDelete(id, function (err) {
    if (err) {
      throw new NotFound(`Note with id ${id} is not found`)
    }
  })
}

module.exports = {
  addNote,
  getNotes,
  getNoteById,
  updateNote,
  updateCheckField,
  deleteNote,
}
