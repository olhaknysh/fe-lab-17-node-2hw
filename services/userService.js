const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const SALT_FACTOR = 6

const { User } = require('../model/userModal.js')
const {
  notAuthorized,
  userWithSuchUsername,
  PasswordError,
  DeleteError,
} = require('../helpers/errors')

const registerUser = async (username, password) => {
  const userWithName = await User.findOne({ username })
  if (userWithName) {
    throw new userWithSuchUsername('Such username is in use')
  }

  const user = new User({
    username,
    password,
  })
  await user.save()
}

const loginUser = async (username, password) => {
  const user = await User.findOne({ username })

  if (!user || !(await bcrypt.compare(password, user.password))) {
    throw new notAuthorized('Username or password is wrong')
  }

  const token = jwt.sign(
    {
      _id: user._id,
    },
    process.env.JWT_SALT
  )
  await user.updateOne({ token: token })

  return token
}

const updateUser = async (id, oldPassword, newPassword) => {
  const user = await User.findById(id)
  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new PasswordError('Old password is wrong')
  }
  user.password = newPassword
  await user.save()
}

const removeUser = async (id) => {
  await User.findByIdAndDelete(id, function (err) {
    if (err) {
      throw new DeleteError('Problem with deleting user')
    }
  })
}

module.exports = {
  registerUser,
  loginUser,
  updateUser,
  removeUser,
}
